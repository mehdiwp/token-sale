App = {
  web3Provider: null,
  contracts: {},
  account: "0x0",
  loading: false,
  tokenPrice: 1000000000000000,
  tokensSold: 0,
  tokensAvailable: 750000,

  init: () => {
    console.log("Appinitialized");
    return App.initWeb3();
  },

  initWeb3: () => {
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    } else {
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
      web3 = new Web3(App.web3Provider);
    }
    return App.initContracts();
  },

  initContracts: () => {
    $.getJSON("DappTokenSale.json", (dappTokenSale) => {
      App.contracts.DappTokenSale = TruffleContract(dappTokenSale);
      App.contracts.DappTokenSale.setProvider(App.web3Provider);
      App.contracts.DappTokenSale.deployed().then((dappTokenSale) => {
        console.log("Dapp Token Sale Address:", dappTokenSale.address);
      })
    }).done(() => {
      $.getJSON('DappToken.json', (dappToken) => {
        App.contracts.DappToken = TruffleContract(dappToken);
        App.contracts.DappToken.setProvider(App.web3Provider);
        App.contracts.DappToken.deployed().then((dappToken) => {
          console.log("Dapp Token Address:", dappToken.address);
        });
        App.listenForEvent();
        return App.render();
      });
    });
  },

  listenForEvent: () => {
    App.contracts.DappTokenSale.deployed().then((instance) => {
      instance.Sell({}, {
        fromBlock: 0,
        toBlock: 'latest',
      }).watch((error, event) => {
        console.log("event trriged", event);
        App.render();
      })
    })
  },

  render: () => {
    if (App.loading) {      
      return
    }
    App.loading = true;

    var loader = $("#loader");
    var content = $("#content");

    loader.show();
    content.hide();

    web3.eth.getCoinbase((err, account) => {
      if(err === null) {
        App.account = account;
        $('#accountAddress').html(`Your Account: ${account}`);
      }
    });

    App.contracts.DappTokenSale.deployed().then((instance) => {
      dappTokenSaleInstance = instance;
      return dappTokenSaleInstance.tokenPrice();
    }).then((tokenPrice) => {
      App.tokenPrice = tokenPrice;
      $(".token-price").html(web3.fromWei(App.tokenPrice, "ether").toNumber());
      return dappTokenSaleInstance.tokensSold();
    }).then((tokensSold) => {
      App.tokensSold = tokensSold.toNumber();
      $(".tokens-sold").html(App.tokensSold);      
      $(".tokens-available").html(App.tokensAvailable); 
      
      var progressPercent = (App.tokensSold / App.tokensAvailable) * 100;
      $("#progress").css("width", progressPercent + "%");
      App.contracts.DappToken.deployed().then((instance) => {
        dappTokenInstnce = instance;
        return dappTokenInstnce.balanceOf(App.account);
      }).then((balance) => {
        $('.dapp-balance').html(balance.toNumber());

        App.loading = false;
        loader.hide();
        content.show();
      })
    })
  },

  buyTokens: () => {
    $("#content").hide();
    $("#loader").show();

    var numberOfTokens = $("#numberOfTokens").val();
    App.contracts.DappTokenSale.deployed().then((instance) => {
      return instance.buyTokens(numberOfTokens, {
        from: App.account,
        value: numberOfTokens * App.tokenPrice,
        gas: 500000
      });
    }).then((result) => {
      console.log("Tokens bought");
      $("form").trigger("reset");
    });
  }

}

$(function () {
  $(window).on('load', function () {
    App.init();
  });
});
